//
//  ViewController.swift
//  Source Control
//
//  Created by Jordan Benator on 7/14/20.
//  Copyright © 2020 Benator. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    /**
     Adds two numbers together and returns the result. Modified.
     - parameter num1: The first number
     - parameter num1: The second number.
     - returns: the sum of num1 and num1.
     */
    func addNumbers(num1: Int, num2: Int) -> Int {
        return num1 + num2
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

